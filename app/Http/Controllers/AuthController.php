<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    //
       
    // public function register(Request $request){

    //     $validate=Validator::make($request->all(),[
    //         'name' 		=> 'required',
    //         'email'	 	=> 'required|email|unique',
    //         'password' 	=> 'required|confirmed'
    //     ]);

    //     $user = User::create([
    //         'password'   => Hash::make($request->password),
    //         'email'      => $request->email,
    //         'name'       => $request->name,
    //     ]);

    //     $token = $user->createToken('auth_token')->plainTextToken;

    //     return response()->json([
    //             'access_token' => $token,
    //             'token_type' => 'Bearer',
    //     ]);
    // }

    public function register(Request $request){
        $token = explode('@',$request->email,2);
        $validate=Validator::make($request->all(),[
            'name' 		=> 'required',
            'email'	 	=> 'required|email|unique',
            'password' 	=> 'required|confirmed'
        ]);

        $user = User::create([
            'password'   => Hash::make($request->password),
            'email'      => $request->email,
            'name'       => $request->name,
            'api_token'       => $token[0],
        ]);

        // $token = $user->createToken('auth_token')->plainTextToken;

        // return response()->json([
        //         'access_token' => $token,
        //         'token_type' => 'Bearer',
        // ]);
        return $user;
    }

    // public function login(Request $request){
    //     $validate=Validator::make($request->all(),[
    //         'email' => 'email|required',
    //         'password' => 'required'
    //     ]);

    //     $credentials = request(['email', 'password']);

    //     if (!Auth::attempt($credentials)) {
    //         return response()->json([
    //             'status_code' => 500,
    //             'message' => 'Unauthorized'
    //         ]);
    //     }

    //     $user = User::where('email', $request->email)->first();

    //     if (!$user || !Hash::check($request->password, $user->password)) {
    //         throw new \Exception('Error in Login');
    //     }

    //     $tokenResult = $user->createToken('authToken')->plainTextToken;

    //     return response()->json([
    //         'access_token' => 'Bearer ' . $tokenResult,
    //     ]);

    // }

    public function login(Request $request){

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return Auth::user();
        }
    }

    public function authResponse(){
        return response()->json([
            'status_code' => 401,
            'message' => 'Unauthorized'
        ]);
    }

}
