<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Uploads;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;



class cURLController extends Controller
{
    //
    public function index(){
        return Uploads::all();
    }

    public function show($id){
        
        $file =  Uploads::find($id)->file_name;
        return  Storage::get('public/uploads/' .$file);
    }


    /**
     * Handle upload file with curl -T $file $url
     */

    // curl -H authorization:'access_token' -T $file $url
    public function uploadWithPut(){
 
            $uuid = Str::uuid();
            $user = request()->user()->email;
            $contents = file_get_contents("php://input");
            $name = 'user=' . request()->user()->id .'_file=' . rand(0, 999999);
            $url = URL::current() . '/' . $uuid ;
            Storage::put('public/uploads/' . $name, $contents);
            $upload = new Uploads();
            $upload->id = $uuid;
            $upload->file_name = $name;
            $upload->user = $user;
            $upload->url = $url;
            $upload->save();
            return  $url;
        
    }

    //curl -H authorization:'$token' -X "DELETE" $file_url
    public function delete($id){
        $file =  Uploads::find($id);

        $file->delete();
        
        Storage::delete('public/upload/' . $file->file_name);
        return "Delete Successfully";
    }






}
