<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Uploads;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/authResponse',[App\Http\Controllers\AuthController::class, 'authResponse'])->name('authResponse');

Route::post('/login',[App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::post('/register',[App\Http\Controllers\AuthController::class, 'register'])->name('register');
Route::get('upload/{id}', [App\Http\Controllers\cURLController::class, 'show']);
Route::post('authResponse',[App\Http\Controllers\AuthController::class, 'authResponse']);


// Route::middleware(['auth:sanctum'])->group(function () {
//     Route::put('upload', [App\Http\Controllers\cURLController::class, 'uploadWithPut']);
//     Route::delete('upload/{id}', [App\Http\Controllers\cURLController::class, 'delete']);
// }); 

Route::middleware(['auth:api'])->group(function () {
    Route::put('upload', [App\Http\Controllers\cURLController::class, 'uploadWithPut']);
    Route::delete('upload/{id}', [App\Http\Controllers\cURLController::class, 'delete']);
}); 





